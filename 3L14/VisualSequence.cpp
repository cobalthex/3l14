#include "VisualSequence.hpp"

void VisualSequence::Update(size_t CurrentFrame, size_t NextFrame, float FramePercent)
{
	auto& cur = visuals[CurrentFrame];
	auto& nxt = visuals[NextFrame];

	if ((visual.hasBounds = nxt.hasBounds))
	{
		visual.bounds.x = Lerp(cur.bounds.x, nxt.bounds.x, FramePercent);
		visual.bounds.y = Lerp(cur.bounds.y, nxt.bounds.y, FramePercent);
		visual.bounds.width = Lerp(cur.bounds.width, nxt.bounds.width, FramePercent);
		visual.bounds.height = Lerp(cur.bounds.height, nxt.bounds.height, FramePercent);
	}

	if ((visual.hasTheta = nxt.hasTheta))
		visual.theta = Lerp(cur.theta, nxt.theta, FramePercent);

	if ((visual.hasColor = nxt.hasColor))
	{
		visual.color.a = Lerp(cur.color.a, nxt.color.a, FramePercent);
		visual.color.r = Lerp(cur.color.r, nxt.color.r, FramePercent);
		visual.color.g = Lerp(cur.color.g, nxt.color.g, FramePercent);
		visual.color.b = Lerp(cur.color.b, nxt.color.b, FramePercent);
	}
}

void VisualSequence::Reset()
{
	Sequence::Reset();
	visual = visuals[0];
}
void VisualSequence::Finish()
{
	Sequence::Finish();

	auto& back = visuals.back();
	if ((visual.hasBounds = back.hasBounds))
		visual.bounds = back.bounds;
	if ((visual.hasTheta = back.hasTheta))
		visual.theta = back.theta;
	if ((visual.hasColor = back.hasColor))
		visual.hasColor = back.hasColor;
}

void VisualSequence::Append(VisualSequence* const Sequence, const TimeSpan & Offset, bool Prepend)
{
	//Add the current visual to the beginning of the new animation (usually used when 'continuing' an animation)
	if (Prepend)
	{
		Keyframe k = keyframes.back();
		k.start = TimePoint(Offset);
		Sequence::Append(k);
		visuals.push_back(visual);
	}
	Sequence::Append(Sequence->keyframes, Offset);
	visuals.insert(visuals.end(), Sequence->visuals.begin(), Sequence->visuals.end());
}

void VisualSequence::Remove(size_t Index)
{
	keyframes.erase(keyframes.begin() + Index);
	visuals.erase(visuals.begin() + Index);
}
void VisualSequence::Remove(const Keyframe& Keyframe)
{
	auto idx = std::find(keyframes.begin(), keyframes.end(), Keyframe);
	keyframes.erase(idx);
	visuals.erase(visuals.begin() + (idx - keyframes.begin()));
}