#pragma once

#include "Event.hpp"

enum class InputType
{
	Unknown,
	Sequence, //A sequence of inputs, contains a list of inputs
	Mouse,
	MouseButton,
	Keyboard
};

struct IInput { };

template <InputType Type>
struct Input : IInput
{
	static inline InputType Type() { return Type; }
};

template <>
struct Input <InputType::Mouse> : IInput
{
	int x, y;

	Input(int X, int Y) : x(X), y(Y) { }
};

//Mouse buttons, can be combined (usually not however)
enum class MouseButtons
{
	Left = 0,
	Middle = 1,
	Right = 2,
	X1 = 4,
	X2 = 8
};
//The state of the button
enum class ButtonState
{
	Released,
	Pressed
};
template <>
struct Input <InputType::MouseButton> : IInput
{
	MouseButtons button;
	ButtonState state;

	Input(MouseButtons Button, ButtonState State) : button(Button), state(State) { }
};

template <>
struct Input <InputType::Keyboard> : IInput
{
	uint32_t key;
	ButtonState state;

	ButtonState ctrlState;
	ButtonState shiftState;
	ButtonState altState;

	Input(uint32_t Key, ButtonState KeyState, ButtonState CtrlKeyState, ButtonState ShiftKeyState, ButtonState AltKeyState)
		: key(Key), state(KeyState), ctrlState(CtrlKeyState), shiftState(ShiftKeyState), altState(AltKeyState) { }
};

//A single event that encapsulates all of the different types of events
struct InputEvent : Event
{
	InputType type;
	IInput* input;

	InputEvent(InputType Type, IInput* const Input, const BaseEventArgs& EvArgs = BaseEventArgs())
		: type(Type), input(Input), Event(EvArgs) { }
};