#pragma once

#include "Component.hpp"
#include "Dimensions.hpp"

class TransformComponent : public Component
{
public:
	Point position = Point(); //Cartesian position of the entity
	float angle = 0; //Angle, in radians
};