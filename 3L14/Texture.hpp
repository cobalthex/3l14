#pragma once

#include "Dimensions.hpp"
#include "Main.hpp"

//standard color channel masks
extern const uint32_t SDL_RMASK, SDL_GMASK, SDL_BMASK, SDL_AMASK;

class Texture
{
public:
	Texture();
	Texture(const Texture& Tex); //copies internal texture
	Texture(SDL_Renderer* Renderer, SDL_Texture* SdlTex); //does not copy SDL texture
	Texture(SDL_Renderer* Renderer, const std::string& File);
	Texture(SDL_Renderer* Renderer, const char* File);
	Texture(SDL_Renderer* Renderer, SDL_Surface* Surface);
	~Texture();

	Texture& operator = (const Texture& Tex);
	inline operator SDL_Texture*() const { return tex.get(); }

	inline virtual bool IsValid() const { return (tex != nullptr); }

	inline SDL_Texture* GetSdlTexture() const { return tex.get(); }

	virtual inline int Width() const { return width; }
	virtual inline int Height() const { return height; }
	virtual inline Rect Bounds() const { return Rect(0, 0, width, height); }

	inline Uint32 Format() const { return format; }
	inline int Access() const { return access; }

	virtual void Draw(const Rect& DestRect) const
	{
		SDL_RenderCopy(renderer, tex.get(), nullptr, (SDL_Rect*)DestRect);
	}
	virtual void Draw(const Rect& DestRect, const Rect& SourceRect) const
	{
		SDL_RenderCopy(renderer, tex.get(), (SDL_Rect*)SourceRect, (SDL_Rect*)DestRect);
	}
	virtual void Draw(const Point& DestPoint, const Rect& SourceRect) const
	{
		SDL_Rect r = { DestPoint.x, DestPoint.y, width, height };
		SDL_RenderCopy(renderer, tex.get(), (SDL_Rect*)SourceRect, &r); //Rect contains cast to SDL_Rect*
	}
	virtual void Draw(const Point& DestPoint) const
	{
		SDL_Rect r = { DestPoint.x, DestPoint.y, width, height };
		SDL_RenderCopy(renderer, tex.get(), nullptr, &r); //Rect contains cast to SDL_Rect*
	}

	//lock for editing and unlock to finalize
	//Resources are managed, do not destroy
	SDL_Surface* Lock(const Rect& Region = Rect::Empty);
	void Unlock();
	inline bool Update(const void* Pixels, int Pitch, const Rect& Region = Rect::Empty);
	bool Update(const void* Pixels, int Pitch, const SDL_Rect* Region = nullptr);

	void Hue(const SDL_Color& Color) const; //Set the hue of the texture (including alpha)
	SDL_Color Hue() const; //Get the hue of the texture (including alpha)

	void Alpha(uint8_t Alpha) const; //Set only the alpha of the texture
	uint8_t Alpha() const; //Get only the alpha of the texture

	void BlendMode(SDL_BlendMode BlendMode) const; //Set the blend mode of the texture
	SDL_BlendMode BlendMode() const; //Get the blend mode of the texture

protected:
	void QueryInfo(); //query the texture and get its info (store it here)
	void SetTexture(SDL_Texture* Tex);

	std::shared_ptr<SDL_Texture> tex;
	SDL_Renderer* renderer;

	int width, height;
	Uint32 format;
	int access;

	//private temporary for locking textures (texture locking must be enabled)
	SDL_Surface* _pixBuf;
};