#pragma once

#include "Font.hpp"
#include "Main.hpp"

#define WINDOWS_FONT_FOLDER "C:\\Windows\\Fonts\\"
#define LINUX_FONT_FOLDER "/usr/local/share/fonts/truetype/"

class TTFFont : public Font
{
public:
	TTFFont(TTF_Font* Font);
	TTFFont(const std::string& FontFile, int Size, int Index = 0);
	~TTFFont();

	inline bool IsValid() const { return font != nullptr; }

protected:
	void GenerateGlyphs(int StartGlyph = 32, int EndGlyph = 255);

	SDL_Surface* glyphs;
	TTF_Font* font;
	bool _ownAlloc; //font was opened locally
};

