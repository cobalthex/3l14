#include "Component.hpp"

//A collection of components, stored by their type
//Note, one component collection per entity
class ComponentCollection
{
public:
	typedef std::map<std::type_index, std::shared_ptr<Component>> ComponentMap;

	ComponentCollection(Entity* const Entity) : entity(Entity) { }
	virtual ~ComponentCollection() { }

	//Create a new component and return it
	template <class ComponentType>
	std::shared_ptr<ComponentType> Create()
	{
		Component* assert_ComponentType_is_Component = ((ComponentType*)nullptr);

		auto cp = std::make_shared<ComponentType>();
		components[typeid(ComponentType)] = cp;
		cp->entity = entity;
		return cp;
	}
	//Add an existing component (sets ownership)
	template <class ComponentType>
	void Add(const std::shared_ptr<ComponentType>& Component)
	{
		::Component* assert_ComponentType_is_Component = ((ComponentType*)nullptr);

		components[typeid(ComponentType)] = Component;
		Component->entity = entity;
	}

	std::shared_ptr<Component> operator[](const std::type_index& Type) const
	{
		if (components.find(Type) == components.end())
			return nullptr;

		auto c = components.at(Type);
		return c;
	}
	template <class ComponentType>
	inline std::shared_ptr<ComponentType> Get() const
	{
		std::type_index ti = typeid(ComponentType);
		return std::static_pointer_cast<ComponentType>(operator[](ti));
	}

	template <class ComponentType>
	inline std::shared_ptr<ComponentType> Has() const
	{
		std::type_index ti = typeid(ComponentType);
		return (components.find(ti) != components.end());
	}

	inline ComponentMap& data() { return components; }
	inline const ComponentMap& data() const { return components; }

protected:
	ComponentMap components;
	Entity* entity; //the entity that owns this component collection
};