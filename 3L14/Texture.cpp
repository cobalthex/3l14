#include "Texture.hpp"
#include "Logger.hpp"

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
uint32_t const SDL_RMASK = 0xff000000;
uint32_t const SDL_GMASK = 0x00ff0000;
uint32_t const SDL_BMASK = 0x0000ff00;
uint32_t const SDL_AMASK = 0x000000ff;
#else
uint32_t const SDL_RMASK = 0x000000ff;
uint32_t const SDL_GMASK = 0x0000ff00;
uint32_t const SDL_BMASK = 0x00ff0000;
uint32_t const SDL_AMASK = 0xff000000;
#endif


Texture::Texture()
{
	tex = nullptr;
	renderer = nullptr;
	_pixBuf = nullptr;
}
Texture::Texture(const Texture& Tex)
{
	*this = Tex;
	_pixBuf = nullptr;
}
Texture::Texture(SDL_Renderer* Renderer, SDL_Texture* SdlTex)
{
	_pixBuf = nullptr;
	renderer = Renderer;
	SetTexture(SdlTex);
	QueryInfo();
}
Texture::Texture(SDL_Renderer* Renderer, const std::string& File)
{
	_pixBuf = nullptr;
	renderer = Renderer;
	SetTexture(IMG_LoadTexture(renderer, File.data()));
	QueryInfo();
}
Texture::Texture(SDL_Renderer* Renderer, const char* File)
{
	_pixBuf = nullptr;
	renderer = Renderer;
	SetTexture(IMG_LoadTexture(renderer, File));
	QueryInfo();
}
Texture::Texture(SDL_Renderer* Renderer, SDL_Surface* Surface)
{
	_pixBuf = nullptr;
	renderer = Renderer;
	SetTexture(SDL_CreateTextureFromSurface(Renderer, Surface));
	QueryInfo();
}

Texture::~Texture() { }

void Texture::SetTexture(SDL_Texture* Tex)
{

	auto deltr = [](SDL_Texture* Tex) { SDL_DestroyTexture(Tex); };
	tex = std::shared_ptr<SDL_Texture>(Tex, deltr);
}

Texture& Texture::operator = (const Texture& Tex)
{
	if (&Tex != this && tex != Tex.tex)
	{
		if (tex != nullptr)
		{
			tex.reset();
			tex = nullptr;
			delete _pixBuf;
			_pixBuf = nullptr;
		}

		renderer = Tex.renderer;
		format = Tex.format;
		access = Tex.access;
		width = Tex.width;
		height = Tex.height;
		_pixBuf = Tex._pixBuf;

		tex = Tex.tex;
	}

	return *this;
}

void Texture::Hue(const SDL_Color& Color) const
{
	SDL_SetTextureColorMod(tex.get(), Color.r, Color.g, Color.b);
	SDL_SetTextureAlphaMod(tex.get(), Color.a);
}
SDL_Color Texture::Hue() const
{
	SDL_Color c;
	SDL_GetTextureColorMod(tex.get(), &c.r, &c.g, &c.b);
	SDL_GetTextureAlphaMod(tex.get(), &c.a);
	return c;
}
void Texture::Alpha(uint8_t Alpha) const
{
	SDL_SetTextureAlphaMod(tex.get(), Alpha);
}
uint8_t Texture::Alpha() const
{
	uint8_t alpha;
	SDL_GetTextureAlphaMod(tex.get(), &alpha);
	return alpha;
}
void Texture::BlendMode(SDL_BlendMode BlendMode) const
{
	SDL_SetTextureBlendMode(tex.get(), BlendMode);
}
SDL_BlendMode Texture::BlendMode() const
{
	SDL_BlendMode b;
	SDL_GetTextureBlendMode(tex.get(), &b);
	return b;
}

void Texture::QueryInfo()
{
	if (tex == nullptr)
		return;

	SDL_QueryTexture(tex.get(), &format, &access, &width, &height);
}

SDL_Surface* Texture::Lock(const Rect& Region)
{
	if (_pixBuf != nullptr)
		return _pixBuf; //already locked

	void* pixels;
	int pitch;

	//error locking
	SDL_Rect* r = (Region.Equals(Rect::Empty) ? nullptr : &(SDL_Rect)Region.Intersection(Bounds()));
	if (SDL_LockTexture(tex.get(), r, &pixels, &pitch) < 0)
		return nullptr;

	if (r == nullptr)
		_pixBuf = SDL_CreateRGBSurfaceFrom(pixels, width, height, 32, pitch, SDL_RMASK, SDL_GMASK, SDL_BMASK, SDL_AMASK);
	else
		_pixBuf = SDL_CreateRGBSurfaceFrom(pixels, r->w, r->h, 32, pitch, SDL_RMASK, SDL_GMASK, SDL_BMASK, SDL_AMASK);

	return _pixBuf;
}

void Texture::Unlock()
{
	if (_pixBuf != nullptr)
	{
		SDL_UnlockTexture(tex.get());
		SDL_FreeSurface(_pixBuf);
		_pixBuf = nullptr;
	}
}

bool Texture::Update(const void* Pixels, int Pitch, const Rect& Region)
{
	return Update(Pixels, Pitch, Region.Equals(Rect::Empty) ? nullptr : &(SDL_Rect)Region.Intersection(Bounds()));
}
bool Texture::Update(const void* Pixels, int Pitch, const SDL_Rect* Region)
{
	int retval = SDL_UpdateTexture(tex.get(), Region, Pixels, Pitch);
	if (retval < 0)
	{
		Logger << SDL_GetError();
		return false;
	}
	return true;
}