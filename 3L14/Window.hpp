#pragma once

#include "Main.hpp"

typedef std::chrono::time_point<std::chrono::high_resolution_clock> HiresTimeStep;
typedef std::chrono::nanoseconds HiresElapsed;

//Graphics window for the game

class Window
{
public:
	Window(std::string Title, unsigned WindowWidth, unsigned WindowHeight, 
		SDL_WindowFlags WindowFlags = (SDL_WindowFlags)0, SDL_RendererFlags RenderFlags = (SDL_RendererFlags)0);
	~Window();

	inline bool IsValid() const { return _valid; }
	inline SDL_Window* GetSdlWindow() const { return wnd; }
	inline SDL_Renderer* GetRenderer() const { return screen; }

	virtual bool Update(); //update inputs and such -- returns false on quit
	virtual void Draw();

	inline void RefreshTimer(); //refresh the timer
	inline HiresElapsed Elapsed() const; //get the elapsed time since the last refresh (RefreshTimer() or Update())

	inline void Close() { _closing = true; }

	inline int Width()
	{
		static int w;
		SDL_GetWindowSize(wnd, &w, 0);
		return w;
	}
	inline int Height()
	{
		static int h;
		SDL_GetWindowSize(wnd, 0, &h);
		return h;
	}

protected:
	SDL_Window* wnd;
	SDL_Renderer* screen;

	bool _valid;

	HiresTimeStep timer;

private:
	bool _closing;
};

