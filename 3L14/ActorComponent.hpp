#pragma once

#include "Component.hpp"
#include "Weapon.hpp"

//The current state of the actor (alive, dead, etc)
enum class ActorState
{
	Dead,
	Alive,
	Dying,
	Resurrecting
};

//The faction to which the actor belongs (defaults to self)
enum class ActorFaction
{
	Self, //Not part of any faction, only cares about itself
	Type, //Only allies to actors of the same type
	Player, //Allies with the player

};

class ActorComponent : public Component
{
	int health;
	int maxHealth; //if max health is 0, actor is invincible

	ActorFaction faction;

	ActorState state;

	std::shared_ptr<Weapon> weapon;
};