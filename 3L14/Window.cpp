#include "Window.hpp"
#include "Logger.hpp"
#include "InputHandler.hpp"

Window::Window(std::string Title, unsigned WindowWidth, unsigned WindowHeight, SDL_WindowFlags WindowFlags, SDL_RendererFlags RenderFlags)
	: _closing(false)
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		Logger << "Error initializing SDL (Video)";
		_valid = false;
		return;
	}
	if (TTF_Init() < 0)
		Logger << "Error initializing TTF";

	wnd = SDL_CreateWindow(Title.data(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WindowWidth, WindowHeight, WindowFlags);
	if (wnd == nullptr)
	{
		Logger << "Error creating SDL Window";
		_valid = false;
		return;
	}

	screen = SDL_CreateRenderer(wnd, -1, RenderFlags);
	
	SDL_SetRenderDrawColor(screen, 0, 0, 0, SDL_ALPHA_OPAQUE);
	SDL_RenderClear(screen);

	_valid = true;
	Logger << "Created window";
}

void Window::RefreshTimer()
{
	timer = std::chrono::high_resolution_clock::now();
}
HiresElapsed Window::Elapsed() const
{
	HiresTimeStep now = std::chrono::high_resolution_clock::now();
	return std::chrono::duration_cast<std::chrono::nanoseconds>(now.time_since_epoch() - timer.time_since_epoch());
}

Window::~Window()
{
	SDL_DestroyRenderer(screen);
	SDL_DestroyWindow(wnd);
	SDL_Quit();
	_valid = false;
}

bool Window::Update()
{
	RefreshTimer();
	if (_closing)
		return false;

	return InputHandler::Update();
}
void Window::Draw()
{
	SDL_RenderPresent(screen);
}