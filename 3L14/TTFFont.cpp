#include "TTFFont.hpp"
#include "Logger.hpp"

TTFFont::TTFFont(TTF_Font* Font)
{
	font = Font;
	_ownAlloc = false;

	glyphs = nullptr;
	GenerateGlyphs();
}
TTFFont::TTFFont(const std::string& FontFile, int Size, int Index)
{
	font = TTF_OpenFontIndex(FontFile.data(), Size, Index);
	_ownAlloc = true;

	if (font == nullptr)
		Logger << TTF_GetError();

	glyphs = nullptr;
	GenerateGlyphs();
}

void TTFFont::GenerateGlyphs(int StartGlyph, int EndGlyph)
{
	if (glyphs != nullptr)
		SDL_FreeSurface(glyphs);

	if (font == nullptr)
		return;

	int numGlyphs = (EndGlyph - StartGlyph), glyphSquare = (int)sqrtf(numGlyphs);
	int width = 0, height = TTF_FontHeight(font) * glyphSquare;

	unsigned rmask = 0x00ff0000;
	unsigned gmask = 0x0000ff00;
	unsigned bmask = 0x000000ff;
	unsigned amask = 0xff000000;
	SDL_CreateRGBSurface(0, width, height, 32, rmask, gmask, bmask, amask);

}

TTFFont::~TTFFont()
{
	if (_ownAlloc)
		TTF_CloseFont(font);
	font = nullptr;
}
//TODO: Use FreeType for more control
