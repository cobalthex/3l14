#pragma once

#include "Dimensions.hpp"
#include "Texture.hpp"
#include "Sequence.hpp"
#include "ComponentCollection.hpp"

typedef std::string EntState;

//Entity base class
class Entity
{
public:
	Entity() : isActive(false), id(0), components(this) { }
	virtual ~Entity() { }

	//Activate this entity and give it a unique ID
	virtual inline void Spawn()
	{
		isActive = true;
		id = ++idCount;
	}

	id_t id;
	bool isActive; //is the entity active?

	ComponentCollection components;
	inline void UpdateComponents() const
	{
		for (auto& c : components.data())
			c.second->Update();
	}

private:
	id_t idCount;
};