#include "Sprite.hpp"

void Sprite::Draw(const Rect& Bounds, const Rect& ClipRect) const
{
	if (texture == nullptr)
		return;

	int fr = (int)Frame();
	Rect source = GetFrameRect(fr, ClipRect);

	bool isLast = fr == (frameCount - 1);
	if (tweening == Tweening::FadeOver && (isLooping || !isLast))
	{
		int nfr = (fr + 1) % frameCount;
		Rect nsource = GetFrameRect(nfr, ClipRect);

		float p = KeyframePercent();

		//fade next in
		texture->Alpha(p <= 0.5f ? Lerp((uint8_t)0, alpha, p * 2) : alpha);
		texture->Draw(Bounds, nsource);

		//fade current out
		texture->Alpha(p >= 0.5f ? Lerp((uint8_t)0, alpha, 1 - (p * 2)) : alpha);
		texture->Draw(Bounds, source);
	}
	else if (tweening == Tweening::FadeThrough && (isLooping || !isLast))
	{
		int nfr = (fr + 1) % frameCount;
		Rect nsource = GetFrameRect(nfr, ClipRect);

		float p = KeyframePercent();

		//fade next in
		texture->Alpha(Lerp((uint8_t)0, alpha, p));
		texture->Draw(Bounds, nsource);

		//fade current out
		texture->Alpha(Lerp((uint8_t)0, alpha, 1 - p));
		texture->Draw(Bounds, source);
	}
	else
	{
		if (tweening != Tweening::None)
			texture->Alpha(alpha);
		texture->Draw(Bounds, source);
	}
}

Rect Sprite::GetFrameRect(size_t Frame, const Rect& ClipRect) const
{
	Rect source = Rect((int)(Frame % columns) * size.x + sourceRect.x, (int)(Frame / columns) * size.y + sourceRect.y, size.x, size.y);
	source.x += ClipRect.x;
	source.y += ClipRect.y;
	source.width = ClipRect.width;
	source.height = ClipRect.height;
	return source;
}

float Sprite::KeyframePercent() const
{
	auto fr = TimePoint(frameLength * Frame());
	auto tc = std::chrono::duration_cast<TimeType>(TimePoint(Time()) - fr).count();
	auto max = std::chrono::duration_cast<TimeType>((fr + frameLength) - fr).count();

	return (tc / (float)max);
}

//Calculate the number of frames given the texture and other info
int Sprite::CalculateFrameCount()
{
	if (texture == nullptr || size.x == 0 || size.y == 0 || sourceRect.width == 0 || sourceRect.height == 0)
		frameCount = 0;
	else
	{
		columns = sourceRect.width / size.x;
		rows = sourceRect.height / size.y;

		frameCount = columns * rows;
	}

	return frameCount;
}