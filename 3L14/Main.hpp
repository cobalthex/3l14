#pragma once

//This is the precompiled header for the project

#pragma region System Libraries

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_mixer.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_net.h>

#pragma endregion


#pragma region Standard Libraries

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <bitset>
#include <queue>
#include <deque>
#include <map>
#include <set>
#include <new>
#include <memory>
#include <typeindex>
#include <forward_list>
#include <chrono>
#include <cstdint>
#include <functional>
#include <algorithm>
#include <memory>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>

#pragma endregion

typedef size_t id_t; //ID storage type