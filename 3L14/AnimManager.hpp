#pragma once

#include "Animation.hpp"
#include <vector>

class AnimManager
{
public:
	bool removeWhenDone; //automatically remove animations that have finished
	bool destroyOnRemove; //Delete the pointers to the animations when they are removed

	AnimManager(bool RemoveWhenDone = true, bool DestroyOnRemove = true);
	~AnimManager();

	void Update(); //Runs through all animations and calls any events if necessary

	inline void Add(Animation* const Animation) { animations.push_back(Animation); }
	void Remove(Animation* const Animation);
	inline void Clear() { animations.clear(); } //Remove all animations

	inline std::vector<Animation>::size_type Count() const { return animations.size(); }
	inline bool IsEmpty() const { return Count() == 0; }

protected:
	void RemoveAt(int Position);

	std::vector<Animation* const> animations;
};

