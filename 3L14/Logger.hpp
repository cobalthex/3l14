#pragma once

#include "Main.hpp"

//Core logging mechanism

class SysLogger
{
public:
	SysLogger();
	~SysLogger();

	static std::string GetTime();

	template <class T>
	SysLogger& operator << (const T& Object)
	{
		std::clog << Object;
		return *this;
	}
};

#define Logger SysLogger()