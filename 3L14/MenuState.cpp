#include "MenuState.hpp"
#include "InputHandler.hpp"
#include "Logger.hpp"
#include "GameStateManager.hpp"
#include "AiTestState.hpp"
#include "TestState.hpp"

MenuState::MenuState()
	: t(Timeline(false))
{
	state = StateState::Intro;

	menus.push_back(
	{
		"Play",
		[this]
		{
			selected = 0;
			state = StateState::Outro;
			menus[0].animation.visual.color.g = 0;
			menus[1].animation.Append(&fadeOutAnim, t.Time() + TimeSpan(200));
			menus[2].animation.Append(&fadeOutAnim, t.Time());
		}
	});
	menus.push_back(
	{
		"Options"
	});
	menus.push_back(
	{
		"Exit",
		[this]
		{
			selected = 2;
			state = StateState::Outro;
			menus[2].animation.visual.color.g = 0;
			menus[1].animation.Append(&fadeOutAnim, t.Time() + TimeSpan(200));
			menus[0].animation.Append(&fadeOutAnim, t.Time());
		}
	});
}

void MenuState::Load(SDL_Renderer* Renderer)
{
	img = Texture(Renderer, "media/Hexafore.png");

	fnt = TTF_OpenFont("media/fixed.ttf", 48);
	SDL_Color c = { 255, 255, 255, 255 };
	if (fnt == nullptr)
		Logger << "Error loading font";
	else
	{
		for (auto& i : menus)
		{
			SDL_Surface* txt = TTF_RenderText_Blended(fnt, i.text.data(), c);
			i.tex = new Texture(Renderer, SDL_CreateTextureFromSurface(Renderer, txt));
			i.visualState = VisualState::Normal;
			SDL_FreeSurface(txt);
		}
	}

	SDL_Surface* srf = TTF_RenderText_Blended(fnt, "        ", c);
	SDL_FreeSurface(srf);

	Keyframe k;
	Visual v;

	//fade-in animation
	k.start = TimePoint(TimeType(0));
	v.color = { 0, 0, 0, 0 };
	v.hasColor = true;
	fadeInAnim.Append(k, v);
	k.start = TimePoint(TimeType(300));
	v.color.a = 255;
	fadeInAnim.Append(k, v);

	//hover animation (note: must use prepend (of Append()) when adding these)
	k.start = TimePoint(TimeType(200));
	v.color.r = 255;
	v.color.g = 100;
	hoverAnim.Append(k, v);

	//un-hover animation
	k.start = TimePoint(TimeType(200));
	v.color.r = 0;
	v.color.g = 0;
	unhoverAnim.Append(k, v);

	//fade-out animatino
	k.start = TimePoint(TimeType(500));
	v.color.r = 200;
	v.color.b = 0;
	v.color.a = 0;
	fadeOutAnim.Append(k, v);
	k.start = TimePoint(TimeType(600));
	v.hasColor = false;
	fadeOutAnim.Append(k, v);

	//set menu animations
	menus[0].animation = fadeInAnim;
	menus[0].animation.Shift(TimeSpan(100));
	menus[0].animation.visual.bounds = Rect(100, 300, menus[0].tex->Width(), menus[0].tex->Height());
	t.Add(&menus[0].animation);

	menus[1].animation = fadeInAnim;
	menus[1].animation.Shift(TimeSpan(300));
	menus[1].animation.visual.bounds = Rect(100, 400, menus[1].tex->Width(), menus[1].tex->Height());
	t.Add(&menus[1].animation);

	menus[2].animation = fadeInAnim;
	menus[2].animation.Shift(TimeSpan(500));
	menus[2].animation.visual.bounds = Rect(100, 500, menus[2].tex->Width(), menus[2].tex->Height());
	t.Add(&menus[2].animation);

	/*SDL_Surface* s = SDL_CreateRGBSurface(0, 200, 200, 32, SDL_RMASK, SDL_GMASK, SDL_BMASK, SDL_AMASK);
	SDL_FillRect(s, &s->clip_rect, 0xffff0000);
	img.Update(s->pixels, s->pitch, &s->clip_rect);*/

	//todo: store temp SDL surface in texture and delete on unlock and reference if lock called multiple times

	t.events.handlers.Add([&](const TimelineEvent& ev)
	{
		if (state == StateState::Outro && ev.type == TimelineEventType::TimelineCompletion)
		{
			if (selected == 0)
				manager->Set<AiTestState>();
			else if (selected == 2)
				manager->ExitGame();
		}
	});
	t.Start();

	//if (InputHandler::mousePressed)
	inputHandlerFunc = InputHandler::events.handlers.Add([&](const InputEvent& ev)
	{
		if (ev.type == InputType::MouseButton)
		{
			auto inp = static_cast<Input<InputType::MouseButton>*>(ev.input);
			if (inp->button != MouseButtons::Left || inp->state != ButtonState::Pressed)
				return;

			if (!t.AtEnd() && state != StateState::Outro)
				t.Finish();
			else
			{
				for (auto& i : menus)
					if (i.action != nullptr && i.animation.visual.bounds.Contains(InputHandler::mouse))
						i.action();
			}
		}
		if (ev.type == InputType::Keyboard)
		{
			auto inp = static_cast<Input<InputType::Keyboard>*>(ev.input);
			if (inp->key == SDLK_t && inp->state == ButtonState::Pressed)
				manager->Set<TestState>();

			if (state != StateState::Outro && inp->key >= SDLK_1 && inp->key <= SDLK_1 + menus.size())
				menus[inp->key - SDLK_1].action();
		}
	});
}
void MenuState::Unload()
{
	if (fnt != nullptr)
		TTF_CloseFont(fnt);

	InputHandler::events.handlers -= inputHandlerFunc;
}

int fc = 0;
void MenuState::Update()
{
	if (InputHandler::keys[SDLK_ESCAPE])
	{
		SDL_Event ev = { SDL_QUIT };
		SDL_PushEvent(&ev);
	}

	auto curt = Time();

	t.Update();

	auto tt = t.Time();

	if (InputHandler::keys[SDLK_z])
	{
		if (t.IsRunning())
			t.Stop();
		else
			t.Start();
		InputHandler::keys[SDLK_z] = false;
	}
	if (InputHandler::keys[SDLK_r])
	{
		t.Restart();
		InputHandler::keys[SDLK_r] = false;
	}

	if (state == StateState::Intro && t.AtEnd())
		state = StateState::Normal;

	if (state == StateState::Normal)
	{
		for (auto& i : menus)
		{
			if (i.animation.visual.bounds.Contains(InputHandler::mouse))
			{
				if (i.visualState == VisualState::Normal)
					i.animation.Append(&hoverAnim, t.Time());
				i.visualState = VisualState::Hover;
			}
			else
			{
				if (i.visualState != VisualState::Normal)
					i.animation.Append(&unhoverAnim, t.Time());
				i.visualState = VisualState::Normal;
			}
		}
	}
}

void MenuState::Draw(SDL_Renderer* Screen)
{
	SDL_SetRenderDrawColor(Screen, 255, 255, 255, 255);
	SDL_RenderClear(Screen);

	int w, h;
	SDL_GetRendererOutputSize(Screen, &w, &h);

	//img.Render(nullptr, nullptr, &Rect(20, 20, img.Width() / 2, img.Height() / 2));

	for (auto& i : menus)
	{
		auto tex = i.tex;
		auto a = i.animation;
		SDL_Rect r = a.visual.bounds;
		SDL_Point center = { r.w / 2, r.h / 2 };
		SDL_SetTextureColorMod(tex->GetSdlTexture(), a.visual.color.r, a.visual.color.g, a.visual.color.b);
		SDL_SetTextureAlphaMod(tex->GetSdlTexture(), a.visual.color.a);
		SDL_RenderCopyEx(Screen, tex->GetSdlTexture(), 0, &r, a.visual.theta * 180 / M_PI, &center, SDL_RendererFlip::SDL_FLIP_NONE);
	}
}