#pragma once

#include "GameState.hpp"
#include "FiniteStateMachine.hpp"
#include "EventQueue.hpp"
#include "Texture.hpp"
#include "Dimensions.hpp"
#include "Timeline.hpp"
#include "Main.hpp"
#include "Entity.hpp"
#include "Sprite.hpp"

enum class Action
{
	GotBored, //goes to walking
	GotTired //goes to idle
};
enum class AiState
{
	Idling,
	Walking,
};
struct AiEvent : public Event
{
	std::string action;

	AiEvent(const std::string& Action, const BaseEventArgs& EvArgs = BaseEventArgs()) 
		: action(Action), Event(EvArgs) { }
};

class AiTestState : public GameState
{
public:
	AiTestState() { }
	~AiTestState() { }

	virtual void Load(SDL_Renderer* Renderer);
	virtual void Unload();

	virtual void Update();
	virtual void Draw(SDL_Renderer* Renderer);

private:
	FiniteStateMachine<AiState, Action> ai;
	EventQueue<AiEvent> eq;

	Texture idlingTex, walkingTex;
	Texture playerTex, enemyTex;

	Entity ent;
	Sprite playerSprite, enemySprite;
};