#pragma once

#include "Main.hpp"
#include "Input.hpp"

//A container for mapping inputs to actions
//Actions should map directly to events
template <class ActionType>
class InputMap
{
public:
	inline void Map(IInput* Input, const ActionType& Action)
	{
		inputs.insert(Input, Action);
	}
	//return the action type (as a pointer) or nullptr if not found
	ActionType* const operator[] (IInput* const Input)
	{
		if (inputs.find(Input) == inputs.end())
			return nullptr;
		return &inputs[Input].second;
	}

protected:
	std::map<IInput*, ActionType> inputs; //input => action
};