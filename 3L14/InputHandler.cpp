#include "InputHandler.hpp"

Point InputHandler::mouse;
Uint8 InputHandler::mousePressed;
bool InputHandler::mouseMoved;
	 
std::bitset<323> InputHandler::keys;
bool InputHandler::altPressed;
bool InputHandler::ctrlPressed;
bool InputHandler::shiftPressed;

EventQueue<InputEvent> InputHandler::events;
bool InputHandler::processEvents = true;
PoolAllocator<> InputHandler::inputEventBuffer;
SDL_Event InputHandler::ev;

void InputHandler::Clear()
{
	mouse = Point(0);
	mousePressed = false;
	mouseMoved = false;

	keys.reset();
}

bool InputHandler::Update()
{
	bool cont = true;
	mouseMoved = false;

	events.Clear();
	inputEventBuffer.Clear();

	while (SDL_PollEvent(&ev))
	{
		if (ev.type == SDL_QUIT)
		{
			cont = false;
			break;
		}

		mousePressed = 0;
		if (ev.type == SDL_MOUSEBUTTONDOWN)
		{
			mousePressed = ev.button.button;

			MouseButtons mb;
			switch (mousePressed)
			{
			case SDL_BUTTON_LEFT: mb = MouseButtons::Left; break;
			case SDL_BUTTON_MIDDLE: mb = MouseButtons::Middle; break;
			case SDL_BUTTON_RIGHT: mb = MouseButtons::Right; break;
			case SDL_BUTTON_X1: mb = MouseButtons::X1; break;
			case SDL_BUTTON_X2: mb = MouseButtons::X2; break;
			}
			ButtonState bs = (ev.button.state == SDL_PRESSED ? ButtonState::Pressed : ButtonState::Released);
			
			auto i = inputEventBuffer.Allocate<Input<InputType::MouseButton>>({ mb, bs });
			events.Enqueue(InputEvent(InputType::MouseButton, i));
		}

		if (ev.type == SDL_MOUSEMOTION)
		{
			mouse.x = ev.motion.x;
			mouse.y = ev.motion.y;
			mouseMoved = true;
		}

		if (ev.type == SDL_KEYDOWN)
		{
			keys[ev.key.keysym.sym % keys.size()] = true;

			ctrlPressed = (ev.key.keysym.mod & KMOD_CTRL) > 0;
			shiftPressed = (ev.key.keysym.mod & KMOD_SHIFT) > 0;
			altPressed = (ev.key.keysym.mod & KMOD_ALT) > 0;

			auto i = inputEventBuffer.Allocate<Input<InputType::Keyboard>>({ (uint32_t)ev.key.keysym.sym, ButtonState::Pressed, (ButtonState)ctrlPressed, (ButtonState)shiftPressed, (ButtonState)altPressed });
			events.Enqueue(InputEvent(InputType::Keyboard, i));
		}
		else if (ev.type == SDL_KEYUP)
		{
			if (ev.key.keysym.mod & KMOD_ALT && ev.key.keysym.sym == SDLK_F4) //alt+f4
			{
				cont = false;
				break;
			}
			else if ((unsigned)ev.key.keysym.sym < keys.size())
				keys[ev.key.keysym.sym] = false;

			ctrlPressed = (ev.key.keysym.mod & KMOD_CTRL) > 0;
			altPressed = (ev.key.keysym.mod & KMOD_ALT) > 0;
			shiftPressed = (ev.key.keysym.mod & KMOD_SHIFT) > 0;
		}
	}

	if (processEvents)
		events.Cycle();

	return cont;
}