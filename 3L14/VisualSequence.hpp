#pragma once

#include "Sequence.hpp"

//Animatable visual properties
struct Visual
{
	Rect bounds = Rect();
	float theta = 0;
	SDL_Color color = SDL_Color();

	bool hasBounds = false;
	bool hasTheta = false;
	bool hasColor = false;
};

typedef std::vector<Visual> VisualList;

//An animation that controls visual properties
class VisualSequence : public Sequence
{
public:
	virtual void Update(size_t CurrentFrame, size_t NextFrame, float FramePercent) override;

	virtual void Reset() override;
	virtual void Finish() override;
	virtual inline void Clear() override { Sequence::Clear(); visuals.clear(); }
	virtual inline void Append(const Keyframe& Keyframe, const Visual& Visual) { Sequence::Append(Keyframe); visuals.push_back(Visual); }
	virtual void Append(VisualSequence* const Sequence, const TimeSpan& Offset = TimeSpan(), bool Prepend = true); //prepend: prepend the new sequence with the last value in the keyframe/visual list

	virtual void Remove(size_t Index) override;
	virtual void Remove(const Keyframe& Keyframe) override;

	Visual visual; //the current visual value

	inline const VisualList& Visuals() const { return visuals; }
	inline VisualList& Visuals() { return visuals; }

protected:
	VisualList visuals; //all of the visuals, one per keyframe
};