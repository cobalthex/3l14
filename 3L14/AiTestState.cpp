#include "AiTestState.hpp"
#include "Logger.hpp"
#include "GameStateManager.hpp"
#include "InputHandler.hpp"
#include "TransformComponent.hpp"
#include "SpriteComponent.hpp"

void AiTestState::Load(SDL_Renderer* Renderer)
{
	auto idling = ai.New(AiState::Idling);
	auto walking = ai.New(AiState::Walking);
	ai.PointTo(idling, Action::GotBored, AiState::Walking);
	ai.PointTo(walking, Action::GotTired, AiState::Idling);

	eq.handlers += [this](const AiEvent& Args)
	{
		if (Args.action == "timeout")
		{
			ai.Input(ai.Current()->id == AiState::Walking ? Action::GotTired : Action::GotBored);
			//Logger << "Event type: " << Args.action << " - " << (int)ai.Current()->id;
			eq.Enqueue(AiEvent("timeout", BaseEventArgs(TimeSpan(300))));
		}
	};
	eq.Enqueue(AiEvent("timeout", BaseEventArgs(TimeSpan(300))));

	auto fnt = TTF_OpenFont("media/fixed.ttf", 48);
	SDL_Color c = { 255, 255, 255, 255 };

	auto srf = TTF_RenderText_Blended(fnt, "Idling", c);
	idlingTex= Texture(Renderer, srf);
	SDL_FreeSurface(srf);

	srf = TTF_RenderText_Blended(fnt, "Walking", c);
	walkingTex = Texture(Renderer, srf);
	SDL_FreeSurface(srf);

	playerTex = Texture(Renderer, "media/player.png");
	enemyTex = Texture(Renderer, "media/enemy.png");

	playerSprite = Sprite(&playerTex, Rect(0, 0, 96, 48), 48, 48, TimeSpan(3000));
	playerSprite.isLooping = true;
	playerSprite.tweening = Tweening::FadeOver;

	enemySprite = Sprite(&enemyTex, Rect(0, 32, 256, 64), 64, 64, TimeSpan(75));
	enemySprite.isLooping = false;
	enemySprite.tweening = Tweening::FadeThrough;

	auto compon = ent.components.Create<TransformComponent>();
	compon->position = Point(100, 100);

	auto spc = std::make_shared<SpriteComponent>(playerSprite);
	ent.components.Add(spc);
	 
	TTF_CloseFont(fnt);
}
void AiTestState::Unload()
{

}

void AiTestState::Update()
{
	eq.Cycle();

	ent.UpdateComponents();

	if (InputHandler::keys[SDLK_ESCAPE])
		manager->ExitGame();

	if (InputHandler::mousePressed)
		enemySprite.Restart();

	if (enemySprite.AtEnd())
		enemySprite.alpha = 0;
	else
		enemySprite.alpha = 255;

	if (InputHandler::keys[SDLK_w])
		ent.components.Get<TransformComponent>()->position.y--;
	if (InputHandler::keys[SDLK_s])
		ent.components.Get<TransformComponent>()->position.y++;

	if (InputHandler::keys[SDLK_a])
		ent.components.Get<TransformComponent>()->position.x--;
	if (InputHandler::keys[SDLK_d])
		ent.components.Get<TransformComponent>()->position.x++;
}
void AiTestState::Draw(SDL_Renderer* Screen)
{
	SDL_SetRenderDrawColor(Screen, 0, 127, 255, 10);
	SDL_RenderClear(Screen);

	Point enemyPos(300, 100);

	if (ai.Current()->id == AiState::Idling)
		idlingTex.Draw(Point(10, 10));
	else
		walkingTex.Draw(Point(10, 10));

	enemySprite.Draw(Point(200, 100));

	ent.components.Get<SpriteComponent>()->Draw();
}