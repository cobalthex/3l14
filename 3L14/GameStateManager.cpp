#include "GameStateManager.hpp"

GameStateManager::GameStateManager(Window* Window)
	: window(Window), nextState(nullptr) { }
GameStateManager::~GameStateManager()
{
	Clear();
}

void GameStateManager::Clear()
{
	while (!states.empty())
		Pop();
}

void GameStateManager::Set(const std::shared_ptr<GameState>& State)
{
	nextState = State;
}
void GameStateManager::Push(const std::shared_ptr<GameState>& State)
{
	states.push_back(State);
	State->manager = this;
	State->startTime = Timer::now();
	State->Load(window->GetRenderer());
}
std::shared_ptr<GameState> GameStateManager::Pop()
{
	auto back = states.back();
	back->Unload();
	back->startTime = TimePoint::min();
	states.pop_back();
	return back;
}

void GameStateManager::Update()
{
	for (ptrdiff_t i = states.size() - 1; i >= 0; i--)
		states[i]->Update();

	if (nextState != nullptr)
	{
		Clear();
		Push(nextState);
		nextState = nullptr;
	}
}
void GameStateManager::Draw(SDL_Renderer* Screen)
{
	for (auto& i : states)
		i->Draw(Screen);
}