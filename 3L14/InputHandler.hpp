#pragma once

#include "Main.hpp"
#include "Dimensions.hpp"
#include "EventQueue.hpp"
#include "PoolAllocator.hpp"
#include "Input.hpp"

//A simple input handler that both stores input states as well as broadcasting them
class InputHandler
{
public:
	static bool Update();
	static void Clear();

	static Point mouse;
	static Uint8 mousePressed; //0 for none, SDL_BUTTON_XXX for which 
	static bool mouseMoved;

	static std::bitset<323> keys;
	static bool altPressed;
	static bool ctrlPressed;
	static bool shiftPressed;
	
	static EventQueue<InputEvent> events; //events that are fired on input
	static bool processEvents; //should the input handler process events (defaults to true, use no for manual management)

protected:
	static SDL_Event ev; //The SDL event to store the incoming events

	static PoolAllocator<> inputEventBuffer;
};
typedef EventQueue<InputEvent>::HandlerId InputHandlerId; //Alias of event queue handler function. Automatically scoped