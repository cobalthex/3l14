#include "Logger.hpp"
#include <ctime>

SysLogger::SysLogger()
{
	std::clog << GetTime() << " | ";
}
SysLogger::~SysLogger()
{
	std::clog << std::endl;
}

std::string SysLogger::GetTime()
{
	time_t t = time(0);
	tm lt;
	localtime_s(&lt, &t);

	std::string ts (8, ':');

	ts[0] = lt.tm_hour / 10 + '0';
	ts[1] = lt.tm_hour % 10 + '0';
	ts[3] = lt.tm_min / 10 + '0';
	ts[4] = lt.tm_min % 10 + '0';
	ts[6] = lt.tm_sec / 10 + '0';
	ts[7] = lt.tm_sec % 10 + '0';

	return ts;
}