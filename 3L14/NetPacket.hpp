#include "Main.hpp"

//A basic packet of data to be transmitted across network(s)
template <class DataType>
struct NetPacket
{
	//Sequences for packets broken up. All packets in the same sequence should have the same time
	union
	{
		struct
		{
			uint8_t isSequence;
			uint8_t sequenceNumber;
		};
		short sequence;
	};
	uint64_t time; //When this packet was sent, queue orders by time. User defined time type

	const uint16_t size; //the size of data in bytes
	DataType data;

	NetPacket() : size((uint16_t)sizeof(DataType)) { }
};