#pragma once

#include "Main.hpp"
#include "GameState.hpp"
#include "Texture.hpp"
#include "Timeline.hpp"

class NetState : public GameState
{
public:
	NetState() { }
	~NetState() { }

	virtual void Load(SDL_Renderer* Renderer);
	virtual void Unload();

	virtual void Update();
	virtual void Draw(SDL_Renderer* Screen);

private:
	IPaddress ip; //server IP
	TCPsocket sock;
	bool good;
	static const size_t sockBufferSz = 512;
	char sockBuffer[sockBufferSz];
	int recvLength;
};