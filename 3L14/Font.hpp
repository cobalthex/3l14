#pragma once

#include "Texture.hpp"
#include "Dimensions.hpp"
#include "Main.hpp"

//Metrics for a single glyph (does not include character)
struct Glyph
{
	Rect size;

};

//Base class for fonts
class Font
{
public:
	virtual void Render(const std::string& Text, Texture& Texture) = 0;
	virtual void Render(const std::wstring& Text, Texture& Texture) = 0;

	virtual void Render(const std::string& Text, SDL_Surface* Surface) = 0;
	virtual void Render(const std::wstring& Text, SDL_Surface* Surface) = 0;

	virtual unsigned SizeText(const std::string& Text);
	virtual unsigned SizeText(const std::wstring& Text);

protected:
};

