#include "TestState.hpp"
#include "InputHandler.hpp"
#include "GameStateManager.hpp"
#include "MenuState.hpp"

#include "PoolAllocator.hpp"
struct SizeBig { int64_t a, b; int32_t c; };
struct SizeSmall { int64_t a, b; };

void TestState::Load(SDL_Renderer* Renderer)
{
	auto fnt = TTF_OpenFont("media/fixed.ttf", 12);
	SDL_Color c = { 255, 255, 255, 255 };

	auto srf = TTF_RenderText_Blended(fnt, "Test State", c);
	introTextTex = Texture(Renderer, srf);
	SDL_FreeSurface(srf);

	TTF_CloseFont(fnt);

	//--\\

	//test pool allocator

	PoolAllocator<4, 8> alloc;
	Logger << alloc.BlocksInUse() << " (" << alloc.LargestAvailableRegion() << " available)";

	auto a = alloc.Allocate<int>();
	Logger << a << " " << alloc.BlocksInUse();

	auto s = alloc.Allocate<size_t>();
	Logger << s << " " << alloc.BlocksInUse();

	auto sb = alloc.Allocate<SizeBig>();
	Logger << sb << "(" << alloc.SizeInBlocks<SizeBig>() << ") " << alloc.BlocksInUse();

	alloc.Deallocate(sb);
	Logger << alloc.BlocksInUse();

	auto ss = alloc.Allocate<SizeSmall>();
	Logger << alloc.BlocksInUse();

	alloc.Clear();
	Logger << alloc.BlocksInUse();

	/*
	auto jsons = JsonString("Test");
	auto jsonn = JsonNumber(5);
	auto jsonnu = JsonNull();
	auto jsonbt = JsonBool(true);
	auto jsonbf = JsonBool(false);

	auto jsona = JsonArray();
	jsona.values.push_back(&jsonnu);
	jsona.values.push_back(&jsonbt);
	jsona.values.push_back(&jsonbf);

	auto jsono = JsonObject();
	jsono.properties["sub"] = &jsons;
	jsono.properties["null"] = &jsonnu;

	auto jsond = JsonObject();
	jsond.properties["string"] = &jsons;
	jsond.properties["number"] = &jsonn;
	jsond.properties["array"] = &jsona;
	jsond.properties["object"] = &jsono;

	std::ofstream fout;
	fout.open("media/test.json");
	jsond.Save(fout);
	fout.close();*/

	JsonDoc json ("media/test.json");
	json.Write("media/test2.json");
}
void TestState::Unload()
{

}

void TestState::Update()
{
	if (InputHandler::keys[SDLK_ESCAPE])
	{
		manager->Set<MenuState>();
		InputHandler::keys[SDLK_ESCAPE] = false;
	}
}
void TestState::Draw(SDL_Renderer* Screen)
{
	SDL_RenderClear(Screen);
	introTextTex.Draw(Point(10, 10));
}