#include "Json.hpp"

using namespace Json;

void Number::Read(std::istream& Stream)
{
	std::string n = "";
	char pk = 0;

	//negative must come first
	if (Stream.peek() == '-')
		n.append('-', 1);

	while (((pk = Stream.peek()) >= '0' && pk <= '9') || pk == 'e' || pk == 'E' || pk == '.')
	{
		n += pk;
		Stream.get();
	}

	value = std::stod(n, nullptr);
}

void String::Write(std::ostream& Stream) const
{
	Stream.put('"');
	std::string s = EscapeQuotes(value);
	Stream.write(s.data(), s.length());
	Stream.put('"');
}
//Todo: Add support for escape codes (unicode, etc)
void String::Read(std::istream& Stream)
{
	value.clear();
	value.reserve(32); //most strings are short

	char oq = Stream.get(), ch = Stream.get(); //(supports single quotes)
	while (ch != oq)
	{
		value += ch;
		if (ch == '\\')
			value += Stream.get();

		ch = Stream.get();
	}
}
std::string String::EscapeQuotes(std::string String)
{
	size_t loc = 0;
	while ((loc = String.find('"', loc)) != std::string::npos)
		String.replace(loc, 1, "\\\""), loc += 2;

	return String;
}
void Bool::Read(std::istream& Stream)
{
	auto pkVal = Stream.peek();
	if (pkVal == 't' || pkVal == 'T')
		value = true;
	else
		value = false;

	Stream.seekg(value ? 4 : 5, std::ios_base::cur);
}

void Object::Write(std::ostream& Stream) const
{
	Stream.put('{');

	int i = 0, n = properties.size();
	for (auto& p : properties)
	{
		i++;

		//write key
		Stream.put('"');
		Stream.write(p.first.data(), p.first.length());
		Stream.write("\":", 2);

		//write value
		p.second->Write(Stream);

		//write encloser
		if (i != n)
			Stream.put(',');
	}
	Stream.put('}');
}
void Object::Read(std::istream& Stream)
{
	Stream.get(); //read {
	while (!Stream.eof() && Stream.peek() != '}')
	{
		SkipWhitespace(Stream);
		//skip comma
		if (Stream.peek() == ',')
		{
			Stream.get();
			SkipWhitespace(Stream);
		}

		//read key (supports single quotes)
		std::string key;
		key.reserve(32);

		char oq = Stream.get(), ch = Stream.get();
		while (ch != oq)
		{
			key += ch;
			if (ch == '\\')
				key += Stream.get();

			ch = Stream.get();
		}

		SkipWhitespace(Stream);
		Stream.get(); //read :
		SkipWhitespace(Stream);

		auto type = GuessType(Stream);
		if (type != Types::Invalid)
		{
			auto val = AllocateType(type);
			if (val != nullptr)
			{
				val->Read(Stream);
				properties[key] = val;
			}
		}

		SkipWhitespace(Stream);
	}
	SkipWhitespace(Stream);
	Stream.get(); //read }
}

void Array::Write(std::ostream& Stream) const
{
	Stream.put('[');

	int n = values.size(), nm1 = n - 1;
	for (int i = 0; i < n; i++)
	{
		//write value
		values[i]->Write(Stream);

		//write encloser
		if (i != nm1)
			Stream.put(',');
	}
	Stream.put(']');
}
void Array::Read(std::istream& Stream)
{
	Stream.get(); //read [
	while (!Stream.eof() && Stream.peek() != ']')
	{
		SkipWhitespace(Stream);
		//skip comma
		if (Stream.peek() == ',')
		{
			Stream.get();
			SkipWhitespace(Stream);
		}

		auto type = GuessType(Stream);
		if (type != Types::Invalid)
		{
			auto val = AllocateType(type);
			if (val != nullptr)
			{
				val->Read(Stream);
				values.push_back(val);
			}
		}

		SkipWhitespace(Stream);
	}
	SkipWhitespace(Stream);
	Stream.get(); //read ]
}

void Doc::Read(std::istream& Stream)
{
	SkipWhitespace(Stream);
	auto type = GuessType(Stream);
	root = AllocateType(type);
	if (root != nullptr)
		root->Read(Stream);
}

Types Composite::GuessType(std::istream& Stream)
{
	char pk = Stream.peek();
	if (pk >= '0' && pk < '9')
		return Types::Number;

	switch (pk)
	{
	case '{': return Types::Object;
	case '[': return Types::Array;
	case '.':
	case '-': return Types::Number;
	case 't': 
	case 'T':
	case 'f':
	case 'F': return Types::Boolean;
	case 'n':
	case 'N': return Types::Null;
	case '\'':
	case '"': return Types::String;

	default: return Types::Invalid;
	}
}

Value* Composite::AllocateType(Types Type)
{
	switch (Type)
	{
	case Types::Null: return Allocate<Null>();
	case Types::Number: return Allocate<Number>();
	case Types::String: return Allocate<String>();
	case Types::Boolean: return Allocate<Bool>();
	case Types::Object: return Allocate<Object>();
	case Types::Array: return Allocate<Array>();
	default: return nullptr;
	}
}

void Composite::SkipWhitespace(std::istream& Stream)
{
	char pk;
	while (!Stream.eof() && ((pk = Stream.peek()) == ' ' || pk == '\t' || pk == '\r' || pk == '\n'))
		pk = Stream.get();
}
