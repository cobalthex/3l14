#pragma once

#include "Main.hpp"
#include "PoolAllocator.hpp"

namespace Json
{
	//A simple JSON representation. Doc/Array/Object do most of the heavy lifting
	//Does not have full error checking, may result in unknown behavior if malformed input

	//All of the types that JSON objects can be
	enum class Types
	{
		Invalid, //Unknown or invalid type (Does not have a corresponding Value)
		Null,
		Number,
		Boolean,
		String,
		Array,
		Object
	};

	//A basic JSON value (all types inherit from this)
	struct Value
	{
	public:
		inline Types Type() const { return type; }

		virtual void Write(std::ostream& Stream) const = 0;
		virtual void Read(std::istream& Stream) = 0; //All Readers can assume that the first character is useful

	protected:
		Value(Types Type) : type(Type) { }
		Value(Value&& Other) : type(Other.type) { }

	private:
		Types type;
	};

	struct Null : public Value
	{
	public:
		Null() : Value(Types::Null) { }

		inline virtual void Write(std::ostream& Stream) const override { Stream.write("null", 4); }
		inline virtual void Read(std::istream& Stream) override { Stream.seekg(4, std::ios_base::cur); };
	};

	//A JSON number. Stored as a double
	struct Number : public Value
	{
	public:
		Number() : Number(0) { }
		Number(double Value) : value(Value), Value(Types::Number) { }

		inline operator int() const { return (int)value; }
		inline operator float() const { return (float)value; }

		inline operator double() const { return value; }
		inline operator double&() { return value; }

		inline virtual void Write(std::ostream& Stream) const override { Stream << value; }
		virtual void Read(std::istream& Stream) override;

	protected:
		double value;
	};

	//A JSON string
	struct String : public Value
	{
	public:
		String() : String("") { }
		String(const std::string& Value) : value(Value), Value(Types::String) { }

		inline operator const std::string&() const { return value; }
		inline operator std::string&() { return value; }

		virtual void Write(std::ostream& Stream) const override; //Writes quotes
		virtual void Read(std::istream& Stream) override; //Reads quotes


	protected:
		std::string value;

		static std::string EscapeQuotes(std::string String);
	};

	//A JSON boolean (true/false)
	struct Bool : public Value
	{
	public:
		Bool() : Bool(false) { }
		Bool(bool Value) : value(Value), Value(Types::Boolean) { }

		inline bool Value() const { return value; }
		inline bool& Value() { return value; }

		inline virtual void Write(std::ostream& Stream) const override { Stream.write(value ? "true" : "false", value ? 4 : 5); }
		virtual void Read(std::istream& Stream) override;

	protected:
		bool value;
	};

	//Provides tools for composite json objects (arrays, objects, docs)
	//Note, all values will be destroyed if this composite goes out of scope/is deleted (However, values not allocated by any composites will remain alive)
	class Composite
	{
	protected: //TODO: verify type information more fully
		static Types GuessType(std::istream& Stream); //Guess the type of object (Only looks based on the first character) - Assumes first character at stream pos is usable
		static void SkipWhitespace(std::istream& Stream); //Skip any whitespace (Schema defined whitespace)

		typedef PoolAllocator<sizeof(Value) * 2, 32> PoolAlloc;
		template <class ValueType> ValueType* Allocate() //allocate a new json value
		{
			if (allocs.size() < 1 || allocs.back().LargestAvailableRegion() < allocs.back().SizeInBlocks<ValueType>())
				allocs.emplace_back();

			return allocs.back().Allocate<ValueType>();
		}
		std::vector<PoolAlloc> allocs; //all of the allocators in use by this document (each pool allocator cannot resize itself so we store a list of thems

		//Allocate a certain type of JSON value
		Value* AllocateType(Types Type);

		Composite() : allocs() { }
		Composite(Composite&& Other) : allocs(std::move(Other.allocs)) { }
	};

	//A JSON object, It can store other json values in a mapped format
	//Note, all values will be destroyed if this object goes out of scope/is deleted (However, values not allocated by this object will remain alive)
	class Object : public Value, protected Composite
	{
	public:
		Object() : properties(), Value(Types::Object), Composite() { }
		Object(Object&& Other) : Composite(std::move(Other)), properties(std::move(Other.properties)), Value(std::move(Other)) { }

		std::map<std::string, Value*> properties;

		virtual void Write(std::ostream& Stream) const override; //Writes braces
		virtual void Read(std::istream& Stream) override; //Reads braces
	};

	//A JSON array, it can store other json values in an indexed format
	//Note, all values will be destroyed if this array goes out of scope/is deleted (However, values not allocated by this array will remain alive)
	class Array : public Value, protected Composite
	{
	public:
		Array() : values(), Value(Types::Array), Composite() { }
		Array(Array&& Other) : Composite(std::move(Other)), values(std::move(Other.values)), Value(std::move(Other)) { }

		std::vector<Value*> values;

		virtual void Write(std::ostream& Stream) const override; //Writes brackets
		virtual void Read(std::istream& Stream) override; //Reads brackets
	};

	//The basic JSON document that has a root element, Utilizes its own allocator
	//Note, all values will be destroyed if this document goes out of scope/is deleted (However, values not allocated by this document will remain alive)
	class Doc : protected Composite
	{
	public:
		Doc() : root(nullptr), Composite() { }
		Doc(const std::string& File) : Doc() { Read(File); }
		Doc(std::istream& Stream) : Doc() { Read(Stream); }

		Doc(Doc&& Other) : Composite(std::move(Other)), root(Other.root) { }

		Value* root;

		inline virtual void Write(const std::string& File) const { std::ofstream fout; fout.open(File, std::ios::out); Write(fout); fout.close(); }
		inline virtual void Read(const std::string& File) { std::ifstream fin; fin.open(File, std::ios::in); Read(fin); fin.close(); }

		inline virtual void Write(std::ostream& Stream) const { root->Write(Stream); }
		virtual void Read(std::istream& Stream);
	};
}