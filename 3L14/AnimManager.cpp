#include "AnimManager.hpp"


AnimManager::AnimManager(bool RemoveWhenDone, bool DestroyOnRemove)
: removeWhenDone(RemoveWhenDone), destroyOnRemove(DestroyOnRemove)
{
	while (animations.size() > 0)
		RemoveAt(0);
}


AnimManager::~AnimManager()
{
}

void AnimManager::Remove(Animation* const Animation)
{
	if (destroyOnRemove)
		delete Animation;
	animations.erase(std::find(animations.begin(), animations.end(), Animation));
}
void AnimManager::RemoveAt(int Position)
{
	if (destroyOnRemove)
		delete animations[Position];
	animations.erase(animations.begin() + Position);
}

void AnimManager::Update()
{
	for (unsigned i = 0; i < animations.size(); i++)
	{
		Animation* a = animations[i];

		if (!a->IsRunning())
			continue;

		if (!a->isLooping && a->GetAbsoluteFrame() >= a->numFrames)
		{
			a->Stop();
			RemoveAt(i--);
			continue;
		}

		int f = a->GetFrame();
		if (a->lastFrame != f)
		{
			AnimEventArgs ev(f, animations[i]);
			if (a->onKeyFrame != nullptr)
				a->onKeyFrame(ev);
			if (a->frameEvents.find(f) != a->frameEvents.end())
				a->frameEvents[f](ev);
		}
	}
}