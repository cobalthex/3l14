#include "Dimensions.hpp"

float Point::Length() const
{
	return (float)sqrt((x * x) + (y * y));
}
int Point::LengthSq() const
{
	return (x * x) + (y * y);
}
float Point::Distance(const Point& A, const Point& B)
{
	return (A - B).Length();
}
int Point::DistanceSq(const Point& A, const Point& B)
{
	return (A - B).LengthSq();
}

Point Point::operator+(const Point& Pt) const
{
	return Point(x + Pt.x, y + Pt.y);
}
Point Point::operator-(const Point& Pt) const
{
	return Point(x + Pt.x, y + Pt.y);
}
Point Point::operator*(int Magnitude) const
{
	return Point(x * Magnitude, y * Magnitude);
}
Point Point::operator/(int Magnitude) const
{
	return Point(x / Magnitude, y / Magnitude);
}

// -- \\

const Rect Rect::Empty;

Rect Rect::Intersection(const Rect& A, const Rect& B)
{
	int xu = SDL_max(A.x, B.x);
	int yu = SDL_max(A.y, B.y);
	int xl = SDL_min(A.x + A.width, B.x + B.width);
	int yl = SDL_min(A.y + A.height, B.y + B.height);

	return Rect(xu, yu, xl - xu, yl - yu);
}