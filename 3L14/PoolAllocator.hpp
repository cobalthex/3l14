#pragma once

#include "Main.hpp"

//A simple pool allocator (Not compliant with C++SL version)
//Block size represents the size of a single block. Allocations are made in blocks as single contiguous regions
template <size_t BlockSizeInBytes = sizeof(size_t), size_t NumberOfBlocks = 32>
class PoolAllocator
{
public:
	typedef uint8_t byte_t;

	PoolAllocator()
		: pool(new byte_t[NumberOfBlocks * BlockSizeInBytes]), blocks(), last(0)
	{

	}
	PoolAllocator(PoolAllocator&& PA)
		: pool(PA.pool), blocks(PA.blocks), last(PA.last) { }
	~PoolAllocator()
	{
		delete[] pool;
	}

	//Allocate a new region of memory for T, returns nullptr on error (out of memory for example)
	//By default, allocates with the default constructor
	template <class T>
	T* Allocate(const T& Value = T())
	{
		size_t nb = SizeInBlocks<T>();

		for (size_t i = last; i != last - 1; ++i %= NumberOfBlocks)
		{
			bool ok = true;
			//check for space
			for (size_t j = 0; j < nb && ok; j++)
			{
				if (i + j >= NumberOfBlocks || blocks[j + i] == 1) //cannot loop a single region
				{
					ok = false;
					i += j;
				}
			}
			if (ok)
			{
				//allocate space
				for (size_t j = 0; j < nb; j++)
					blocks[(j + i) % NumberOfBlocks] = 1;

				last = i + nb - 1; //store the end of the item just inserted

				return new (pool + i * BlockSizeInBytes) T(Value);
			}
		}
		return nullptr; //no space available
	}

	//Deallocate an item from this pool. (Must be in the pool). Set Destroy to false to not call ~T()
	template <class T>
	void Deallocate(const T* const Ptr, bool Destroy = true)
	{
		size_t nb = SizeInBlocks<T>();

		size_t loc = ((byte_t*)Ptr - pool) / BlockSizeInBytes;

		//stupid user
		if (loc < 0 || loc + nb >= NumberOfBlocks)
			return;

		//clear the blocks used
		for (size_t i = loc; i < loc + nb; i++)
			blocks[i] = 0;

		if (Destroy)
			Ptr->~T();
	}

	//Clear the pool. Note: does not call destructors on any of the items inside
	void Clear()
	{
		blocks.reset();
		last = 0;
	}

	//Return the number of blocks in use
	inline size_t BlocksInUse() const
	{
		return blocks.count();
	}

	//return the largest available region in the pool. Returns number of blocks
	size_t LargestAvailableRegion() const
	{
		size_t lg = 0, my = 0;
		for (size_t i = 0; i < NumberOfBlocks; i++)
		{
			if (blocks[i] == 1)
				my = 0;
			else
			{
				my++;
				if (my > lg)
					lg = my;
			}
		}
		return lg;
	}

	template <class T>
	inline size_t SizeInBlocks() const
	{
		size_t sz = sizeof(T);

		size_t nb = sz / BlockSizeInBytes;
		if (sz > nb * BlockSizeInBytes)
			nb++;

		return nb;
	}
	template <class T>
	inline size_t SizeInBlocks(const T* const Ptr) const
	{
		if (Ptr == nullptr)
			return 0;

		size_t sz = sizeof(T);

		size_t nb = sz / BlockSizeInBytes;
		if (sz > nb * BlockSizeInBytes)
			nb++;

		return nb;
	}

protected:
	std::bitset<NumberOfBlocks> blocks; //shows which blocks are in use
	byte_t* pool; //store the pool as single bytes
	size_t last; //store the end position of the last item for hopefully quicker finds
};