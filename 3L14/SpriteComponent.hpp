#pragma once

#include "Component.hpp"
#include "Sprite.hpp"
#include "TransformComponent.hpp"
#include "Entity.hpp"

class SpriteComponent : public Component
{
public:
	SpriteComponent() { }
	SpriteComponent(const Sprite& Sprite) : sprite(Sprite), size(Sprite.Size()), clipRect(Rect(0, 0, Sprite.Width(), Sprite.Height())) { }

	Sprite sprite; //the sprite to draw
	Point size; //How large the sprite should be drawn
	Rect clipRect; //The clip rectangle of each frame

	inline void Draw() const //Entity requires transform component
	{
		auto tc = entity->components.Get<TransformComponent>();
		if (tc != nullptr)
			sprite.Draw(Rect(tc->position.x, tc->position.y, size.x, size.y), clipRect);
	}
};