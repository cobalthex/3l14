#pragma once

#include "Event.hpp"
#include "Main.hpp"

template <typename IdType, typename InputType>
struct StateNode
{
	IdType id; //Some way to identify this node
	std::map<InputType, StateNode<IdType, InputType>*> inputs; //possible inputs to other states, no duplicates allowed
};

//A basic DFA state machine
//No final states exist in this machine
template <typename IdType, typename InputType>
class FiniteStateMachine
{
public:
	FiniteStateMachine() : start(nullptr), current(nullptr)  { }
	virtual ~FiniteStateMachine() { }

	StateNode<IdType, InputType>* const Input(const InputType& Input)
	{
		if (current->inputs.count(Input))
			current = current->inputs.find(Input)->second;

		return current;
	}
	inline StateNode<IdType, InputType>* const Current() const { return current; }

	//Reset the state machine by moving back to start
	inline void Reset() { current = start; }
	
	//Clear all states
	void Clear()
	{
		start = current = nullptr;
		graph->clear();
	}

	//Point a node to another node, identified by id (the first found)
	bool PointTo(StateNode<IdType, InputType>* Node, const InputType& Input, const IdType& NextStateId)
	{
		auto& node = graph.find(NextStateId);
		if (node != graph.end())
		{
			Node->inputs[Input] = &node->second;
			return true;
		}
		return false;
	}

	//Add a new node to the graph; returns the new node created
	inline StateNode<IdType, InputType>* New(const IdType& Id)
	{
		graph[Id] = { Id };
		
		if (start == nullptr)
			start = &graph[Id];
		if (current == nullptr)
			current = &graph[Id];

		return &graph[Id];
	}

protected:
	StateNode<IdType, InputType>* start;
	StateNode<IdType, InputType>* current;
	std::map<IdType, StateNode<IdType, InputType>> graph;
};