#pragma once

#include "Main.hpp"
#include "GameState.hpp"
#include "Texture.hpp"
#include "Timeline.hpp"
#include "VisualSequence.hpp"
#include "InputHandler.hpp"

enum class VisualState
{
	Normal,
	Hover,
	Active
};

enum class StateState
{
	Intro,
	Normal,
	Outro
};

struct MenuOption
{
	std::string text;
	std::function<void()> action;
	Texture* tex; //the graphical item for displaying
	VisualSequence animation;
	VisualState visualState;
};

class MenuState : public GameState
{
public:
	MenuState();
	~MenuState() { }

	virtual void Load(SDL_Renderer* Renderer);
	virtual void Unload();

	virtual void Update();
	virtual void Draw(SDL_Renderer* Screen);

private:
	Texture img;
	TTF_Font* fnt;

	Timeline t;
	VisualSequence fadeInAnim, hoverAnim, unhoverAnim, fadeOutAnim;
	
	StateState state;
	ptrdiff_t selected = -1; //the selected menu entry (-1 for none)

	std::vector<MenuOption> menus;

	InputHandlerId inputHandlerFunc;
};