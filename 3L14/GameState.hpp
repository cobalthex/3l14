#pragma once

#include "Event.hpp"
#include "Main.hpp"

class GameStateManager;

class GameState
{
public:
	GameState() { }
	virtual ~GameState() { }

	virtual void Load(SDL_Renderer* Renderer) = 0;
	virtual void Unload() = 0;

	virtual void Update() = 0;
	virtual void Draw(SDL_Renderer* Renderer) = 0;

	inline GameStateManager* GetManager() const { return manager; }

protected:
	TimePoint startTime; //Set to TimePoint::min() when removed
	TimeType Time() const { return std::chrono::duration_cast<TimeType>(Timer::now() - startTime); } //The current time of the state, relative to its start time (Now - startTime)

	GameStateManager* manager;
	friend class GameStateManager;
};