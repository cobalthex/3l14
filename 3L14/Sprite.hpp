#pragma once

#include "Main.hpp"
#include "Dimensions.hpp"
#include "Texture.hpp"
#include "Animation.hpp"

//How to dislay 'in-between' frames
enum class Tweening
{
	None, //No tweening (each frame is sequential)
	FadeOver, //The next frame fades over top of the current frame before the current frame fades out
	FadeThrough, //Both the current and next frames fade out/in (respectively) simultaneously
};

//An animated 2D graphic that uses fixed frame animation
class Sprite : public Animation
{
public:
	Sprite() : texture(nullptr), size(0), sourceRect(), frameCount(0), rows(0), columns(0), frameLength(TimeSpan(1)) { } //Create a new sprite (frame length is set to one as not to error out)
	//Create a new sprite from a region of a texture and each frame's width and height. Note, no width or height may be zero
	Sprite(Texture* const Texture, const Rect& SourceRect, int FrameWidth, int FrameHeight, const TimeSpan& FrameLength)
		: texture(Texture), sourceRect(SourceRect), size(FrameWidth, FrameHeight), rows(0), columns(0), frameLength(FrameLength)
	{
		CalculateFrameCount();
		alpha = texture->Alpha();
	}

	inline int Width() const { return size.x; } //Get the width of each frame of the sprite
	inline void Width(int Width) { size.x = Width; CalculateFrameCount(); } //set the width of each frame of the sprite
	inline int Height() const { return size.y; } //Get the Height of each frame of the sprite
	inline void Height(int Height) { size.y = Height; CalculateFrameCount(); } //Set the Height of each frame of the sprite
	inline Point Size() const { return size; } //Get the size of each frame (Width, Height)
	inline void Size(const Point& Size) { size = Size; CalculateFrameCount(); } //Set the size of each frame (Width, Height)
	inline Rect SourceRect() const { return sourceRect; } //Get the source rectangle of the texture to use
	inline void SourceRect(const Rect& SourceRect) { sourceRect = SourceRect; CalculateFrameCount(); } //set the source rectangle of the texture to use

	void Draw(const Rect& Bounds, const Rect& ClipRect) const; //Draws the sprite while automatically updating the atnimation. Source rect cooresponds to the clip rectangle of the frame
	inline void Draw(const Rect& Bounds) const { Draw(Bounds, Rect(0, 0, size.x, size.y)); }
	inline void Draw(const Point& Position) const { Draw(Rect(Position.x, Position.y, size.x, size.y), Rect(0, 0, size.x, size.y)); }
	//Draw the sprite at a specified position with a specified clipping rectangle.
	//Note, the sprite will be scaled to the frame size
	inline void Draw(const Point& Position, const Rect& ClipRect) const { Draw(Rect(Position.x, Position.y, size.x, size.y), ClipRect); }

	inline TimeSpan Duration() const override { return frameLength * frameCount; }
	inline size_t FrameCount() const { return frameCount; } //Get the number of frames in the animation (automatically calculated)
	inline long long Frame() const { return Clamp<long long>(Time().count() / frameLength.count(), 0, frameCount - 1); } //The current frame

	TimeSpan frameLength; //the length of time of each frame

	Tweening tweening = Tweening::None; //how to display 'in-between' frames (Note, alpha must be set for consistent blending)
	uint8_t alpha = 255; //the 'fully visible' alpha value of the sprite. Used with various tweening types. Defaults to fully opaque (255)

	float KeyframePercent() const; //get the percent between the current and next keyframes of the animation (0 to 1)

	Texture* const Texture() const { return texture; }

protected:
	::Texture* texture;

	int CalculateFrameCount(); //Calculate the number of frames, returns the set value and sets internally
	Rect GetFrameRect(size_t Frame, const Rect& ClipRect) const;

private:
	//use getters/setters to modify
	Point size; //width and height of each frame
	Rect sourceRect;

	size_t frameCount; //the number of frames
	size_t rows, columns; //the number of rows and columns of frames in the image given the source rect
};