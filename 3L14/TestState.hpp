#pragma once

#include "Main.hpp"
#include "GameState.hpp"
#include "Logger.hpp"
#include "Texture.hpp"
#include "Json.hpp"

//A simple place to test various pieces of code
class TestState : public GameState
{
public:
	TestState() { }
	~TestState() { }

	virtual void Load(SDL_Renderer* Renderer);
	virtual void Unload();

	virtual void Update();
	virtual void Draw(SDL_Renderer* Screen);

private:
	Texture introTextTex;

	JsonObject json;
};