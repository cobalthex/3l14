#include "InputHandler.hpp"
#include "InputMap.hpp"
#include "AiTestState.hpp"

template <class ActionType>
class InputActionHandler : public InputHandler
{
public:
	bool Update() override
	{
		bool cont = true;

		while (SDL_PollEvent(&ev))
		{
			if (ev.type == SDL_QUIT)
			{
				cont = false;
				break;
			}
			if (messageQueue == nullptr)
				continue;

			if (ev.type == SDL_MOUSEBUTTONDOWN)
			{
				MouseButtons button;
				switch (ev.button.button)
				{
				case SDL_BUTTON_LEFT: button = MouseButtons::Left; break;
				case SDL_BUTTON_MIDDLE: button = MouseButtons::Middle; break;
				case SDL_BUTTON_RIGHT: button = MouseButtons::Right; break;
				case SDL_BUTTON_X1: button = MouseButtons::X1; break;
				case SDL_BUTTON_X2: button = MouseButtons::X2; break;
				}
				auto action = inputs[Input<InputType::MouseButton>(button, ButtonState::Pressed)];
				if (action != nullptr)
					messageQueue->Enqueue(*action);
			}

		}
		return cont;
	}

	MessageQueue<ActionType>* messageQueue = nullptr;
	InputMap<ActionType> inputs;
};