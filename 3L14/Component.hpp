#pragma once

#include "Main.hpp"

class Entity;

//The base component of a component based entity system
//Each compoent provides a specific set of functionality that is unique and modular
//Components should generally be independent of each other
//Entities may only have one of each type of component
class Component
{
public:
	Component() : isActive(true) { }
	virtual ~Component() { }

	virtual void Update() { }
	
	Entity* entity; //The entity that owns this component

	bool isActive; //is this component active?

	static std::type_index type; //stored type index
};