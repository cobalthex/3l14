#pragma once

//2D Dimensionsian helper classes
#include "Main.hpp"

//2D point
struct Point
{
	int x, y;
	Point() : x(0), y(0) { }
	Point(int Mag) : x(Mag), y(Mag) { }
	Point(int X, int Y) : x(X), y(Y) { }

	inline operator SDL_Point&() const { return *(SDL_Point*)(this); }
	inline operator SDL_Point*() const { return (SDL_Point*)(this); }

	float Length() const;
	int LengthSq() const;
	static float Distance(const Point& A, const Point& B);
	static int DistanceSq(const Point& A, const Point& B);

	Point operator+(const Point& Pt) const;
	Point operator-(const Point& Pt) const;
	Point operator*(int Magnitude) const;
	Point operator/(int Magnitude) const;
};

//2D Rectangle
struct Rect
{
	int x, y, width, height;
	Rect() : x(0), y(0), width(0), height(0) { }
	Rect(int X, int Y, int Width, int Height) : x(X), y(Y), width(Width), height(Height) { }
	Rect(const Point& Point, int Width, int Height) : x(Point.x), y(Point.y), width(Width), height(Height) { }

	inline operator SDL_Rect&() const { return *(SDL_Rect*)(this); }
	inline operator SDL_Rect*() const { return (SDL_Rect*)(this); }
	
	inline bool Contains(const Point& Pt) const { return (Pt.x >= x && Pt.y >= y && Pt.x < x + width && Pt.y < y + height); }
	inline bool Intersects(const Rect& Rct) const { return !(Rct.x > (x + width) || (Rct.x + Rct.width) < x || Rct.y > (y + height) || (Rct.y + Rct.height) < y); }
	inline bool Equals(const Rect& Rct) const { return (Rct.x == x && Rct.y == y && Rct.width == width && Rct.height == height); }

	static Rect Intersection(const Rect& A, const Rect& B);
	inline Rect Intersection(const Rect& Rct) const { return Intersection(*this, Rct); }

	//TODO: add union

	static const Rect Empty;
};

//Linear interpolation
template <typename T>
inline T Lerp(T A, T B, float Percent) { return (T)(A + (B - A) * Percent); }

//Clamp a value between Min and Max
template <typename T>
inline T Clamp(T Value, T Min, T Max) { return std::max(Min, std::min(Value, Max)); }