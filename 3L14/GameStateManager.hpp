#pragma once

#include "GameState.hpp"
#include "Sequence.hpp"
#include "Window.hpp"
#include "Main.hpp"

//Allows for different content to be run on different screens
class GameStateManager
{
public:
	GameStateManager(Window* Window);
	~GameStateManager();

	void Update();
	void Draw(SDL_Renderer* Screen);

	//All will load/unload states
	template <class TGameState>
	void Set() //Remove all existing states and use this one
	{
		GameState* assert_is_GameState = (TGameState*)nullptr;

		nextState = std::shared_ptr<TGameState>(new TGameState());
	}
	template <class TGameState>
	void Push()
	{
		GameState* assert_is_GameState = (TGameState*)nullptr;

		auto state = std::shared_ptr<TGameState>(new TGameState());
		states.push_back(state);
		state->manager = this;
		state->startTime = Timer::now();
		state->Load(window->GetRenderer());
	}

	void Set(const std::shared_ptr<GameState>& State); //Remove all existing states and use this one
	void Push(const std::shared_ptr<GameState>& State); //Remove all existing states and use this one
	
	std::shared_ptr<GameState> Pop();
	void Clear(); //Remove all states

	//Exit the game via SDL
	inline void ExitGame() const
	{
		SDL_Event ev { SDL_QUIT };
		SDL_PushEvent(&ev);
	}

protected:
	std::vector<std::shared_ptr<GameState>> states;
	std::shared_ptr<GameState> nextState; //used in Set(), sets states to the this after the update run has finished
	Window* window;
};

