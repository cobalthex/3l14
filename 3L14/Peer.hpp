#include "Main.hpp"
#include "EventQueue.hpp"

struct NetEvent : public Event
{

};

class Peer
{
public:

protected:
	EventQueue<NetEvent> messages;
	UDPsocket socket;
};