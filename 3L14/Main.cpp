//Matt Hines April 2014

//Compiling requires C++11 and SDL2, SDL2_image, SDL2_ttf, SDL_mixer

#include "Main.hpp"

#include "Window.hpp"
#include "GameStateManager.hpp"
#include "MenuState.hpp"

int main(int ac, char* av[])
{
	Window window ("3L14", 800, 600, (SDL_WindowFlags)0, (SDL_RendererFlags)(SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE));
	GameStateManager sm (&window);
	sm.Push<MenuState>();

	while (window.Update())
	{
		sm.Update();
		sm.Draw(window.GetRenderer());
		window.Draw();
	}

	return 0;
}