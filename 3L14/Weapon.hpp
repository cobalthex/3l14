#pragma once

#include "Time.hpp"
#include "Sprite.hpp"

class Entity;

//A basic weapon
class Weapon
{
public:
	float damage; //damage per shot
	float range; //the maximum distance the weapon's projectile can fly

	TimeSpan shotDelay; //the time delay between shots

	Sprite projectileSprite; //the sprite of the projectile

	std::function<void(Projectile& Projectile)> projectileFunc; //the function which updates the position of the projectile (collision detection is handled externally)
};

//A basic projectile that is fired from a weaon
struct Projectile
{
	Entity* const originator; //who fired the weapon
	Weapon* const weapon; //the weapon that fired this projectile

	Point position;
	float angle;
	float velocity;

	Point initialPosition;
	float initialAngle;
	float initialVelocity;
};